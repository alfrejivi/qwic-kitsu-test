import React from 'react';

import moment from 'moment';

import './characterCard.css';

const CharacterCard = ({character})=>{	
	return (
		<section className="CharacterCard animated fadeIn">
			<div className="CharacterCard-image">
				<img key={`character_${character.id}`} src={character.image || undefined} alt={ character.name } />
			</div>
			<div className="CharacterCard-info">
				<span className="CharacterCard-id">#ID: <b>{ character.id }</b></span>
				<span className="CharacterCard-name">Name: <b>{ character.name }</b></span>				
				<span className="CharacterCard-type">Type: <b>{ character.type }</b></span>				
				<span className="CharacterCard-role">Role: <b>{ character.role }</b></span>				
				<span className="CharacterCard-createdAt">Created at: <b>{ moment(character.createdAt).format('MMMM Do YYYY, h:mm:ss a') }</b></span>				
				<span className="CharacterCard-updatedAt">Updated at: <b>{ moment(character.updatedAt).format('MMMM Do YYYY, h:mm:ss a') }</b></span>				
			</div>
		</section>
	);
}

export default CharacterCard;