import React, { Component } from 'react';

import CharacterCard from '../characterCard/characterCard';

import './characterList.css';

class CharacterList extends Component{
	constructor(props){
		super(props);

		this.state = {
			searchWord: '',
			filteredCharacters: []
		}

		this.search = this.search.bind(this);
		this.getMoreCharacters = this.getMoreCharacters.bind(this);
	}

	getMoreCharacters(){
		const { type, nextPage } = this.props;
		if(nextPage){
			this.props.getCharacters(type, nextPage);
		}
	}

	search(e){
		const { characters } = this.props;
		const searchWord = e.target.value;

		const filteredCharacters = characters.filter(character=>{
			return character.name.toLowerCase().indexOf(searchWord.toLowerCase()) !== -1;
		});

		this.setState({ filteredCharacters, searchWord });
	}

	render(){
		const { characters, type, nextPage, total, loading } = this.props;
		const { filteredCharacters, searchWord } = this.state;		

		const charactersToMap = filteredCharacters.length ? filteredCharacters : searchWord.length ? 'No characters found' : characters;
		const areThereCharacters = Array.isArray(charactersToMap);

		const actions = (areThereCharacters ? 
							(nextPage ? 
								<button 
									className={`CharacterList-actions-getMore ${ loading ? 'disabled' : '' }`}
									disabled={ loading } 
									onClick={this.getMoreCharacters}>{ loading ? "Loading..." : `Get more ${type} characters` }</button> 
								: <span className="CharacterList-actions-noMore">No more { type } characters</span>)
						: '');

		const mappedCharacters = areThereCharacters ? charactersToMap.map((character,index)=>{
			return (
				<CharacterCard key={`character_${index}`} character={character} />
			);
		}) : charactersToMap;

		return (
			<section className="CharacterList">
				<div className="CharacterList-header">
					<div className="CharacterList-header-info">
						<h2 className="CharacterList-title">{ `${type} characters` }</h2>
						<h2 className="CharacterList-count">{ `${charactersToMap.length}/${total} characters` }</h2>
					</div>
					<div className="CharacterList-search">					
						<input 
							className="CharacterList-search-field" 
							placeholder="Search by name..."
							value={ searchWord }
							onChange={ this.search } />
					</div>
					<div className="CharacterList-actions">
						{ actions }
					</div>
				</div>
				<div className="CharacterList-characters">
					{ mappedCharacters }
				</div>
			</section>
		);
	}
}

export default CharacterList;