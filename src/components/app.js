import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getCharacters } from '../actions/kitsuActions';

import CharacterList from './characterList/characterList';

import 'animate.css';
import './app.css';

class App extends Component {
  constructor(props){
    super(props);

    this.getCharacters = this.getCharacters.bind(this);
  }

  componentWillMount(){
    this.props.getCharacters('anime');
    this.props.getCharacters('manga');
  }

  getCharacters(type, nextPage){
    this.props.getCharacters(type, nextPage)
  }

  render() {
    const { 
      animeCharacters, 
      animeNextPage, 
      animeTotal,
      animeCharactersLoading,
      mangaCharacters, 
      mangaNextPage,
      mangaTotal,
      mangaCharactersLoading } = this.props;

    return (
      <section className="App">
        <header className="App-header">
          <h1 className="App-header-title">QWIC - Kitsu Frontend Test</h1>
        </header>        
        <section className="App-characters">
          <CharacterList 
            type="anime" 
            nextPage={ animeNextPage } 
            characters={ animeCharacters } 
            getCharacters={ this.getCharacters }
            total={ animeTotal }
            loading={ animeCharactersLoading } />
          <CharacterList 
            type="manga" 
            nextPage={ mangaNextPage } 
            characters={ mangaCharacters } 
            getCharacters={ this.getCharacters }
            total={ mangaTotal }
            loading={ mangaCharactersLoading } />
        </section>
      </section>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getCharacters    
  }, dispatch);
}

function mapStoreToProps(state){  
  return {
    animeCharacters: state.kitsu.animeCharacters,
    animeNextPage: state.kitsu.animeNextPage,
    animeTotal: state.kitsu.animeTotal,
    animeCharactersLoading: state.kitsu.animeCharactersLoading,
    mangaCharacters: state.kitsu.mangaCharacters,
    mangaNextPage: state.kitsu.mangaNextPage,
    mangaTotal: state.kitsu.mangaTotal,
    mangaCharactersLoading: state.kitsu.mangaCharactersLoading
  };
}

export default connect(mapStoreToProps, mapDispatchToProps)(App);
