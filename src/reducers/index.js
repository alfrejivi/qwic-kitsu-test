import { combineReducers } from 'redux';

import kitsuReducer from './kitsuReducer';

export default combineReducers({
	kitsu: kitsuReducer
});