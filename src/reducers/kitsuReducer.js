import { 
	GET_ANIME_CHARACTERS,
	GET_ANIME_CHARACTERS_FETCHING,
	GET_ANIME_CHARACTERS_ERROR,
	GET_MANGA_CHARACTERS,
	GET_MANGA_CHARACTERS_FETCHING,
	GET_MANGA_CHARACTERS_ERROR } from '../actions/kitsuActions';

function initState(){
	return {
		animeCharacters: [],
		animeCharactersLoading: false,
		animeNextPage: false,
		animeTotal: false,
		mangaCharacters: [],
		mangaCharactersLoading: false,
		mangaNextPage: false,
		mangaTotal: false,
	}
}

function compareBy(field,a,b) {	
  if (a[field] < b[field])
    return -1;
  if (a[field] > b[field])
    return 1;
  return 0;
}

export default function(state = initState(), action){
	switch(action.type){
		case GET_ANIME_CHARACTERS:
			const animeCharacters = [...state.animeCharacters, ...action.payload.characters];
			return Object.assign({}, state, {
				'animeCharacters' : animeCharacters.sort((a,b)=>{return compareBy('name',a,b)}),
				'animeNextPage': action.payload.nextPage,
				'animeTotal': action.payload.total,
				'animeCharactersLoading': false 
			});
		case GET_ANIME_CHARACTERS_FETCHING:
			return Object.assign({}, state, { 'animeCharactersLoading': true } );
		case GET_ANIME_CHARACTERS_ERROR:
			// TODO: handle API error response into the state
			return state;
		case GET_MANGA_CHARACTERS:
			const mangaCharacters = [...state.mangaCharacters, ...action.payload.characters];
			return Object.assign({}, state, {
				'mangaCharacters' : mangaCharacters.sort((a,b)=>{return compareBy('name',a,b)}),
				'mangaNextPage': action.payload.nextPage,
				'mangaTotal': action.payload.total,
				'mangaCharactersLoading': false
			});
		case GET_MANGA_CHARACTERS_FETCHING:
			return Object.assign({}, state, { 'mangaCharactersLoading': true } );
		case GET_MANGA_CHARACTERS_ERROR:
			// TODO: handle API error response into the state
			return state;
		default:
			return state;
	}
}