import axios from 'axios';

import { KITSU_API_URL } from '../general/constants';

export const GET_ANIME_CHARACTERS = 'GET_ANIME_CHARACTERS';
export const GET_ANIME_CHARACTERS_ERROR = 'GET_ANIME_CHARACTERS_ERROR';
export const GET_ANIME_CHARACTERS_FETCHING = 'GET_ANIME_CHARACTERS_FETCHING';
export const GET_MANGA_CHARACTERS = 'GET_MANGA_CHARACTERS';
export const GET_MANGA_CHARACTERS_ERROR = 'GET_MANGA_CHARACTERS_ERROR';
export const GET_MANGA_CHARACTERS_FETCHING = 'GET_MANGA_CHARACTERS_FETCHING';

function parseToDispatch(type, payload = false){
	return { type, payload }
}

function parseCharacters({ data,included }){
	const parsedCharacters = included.map((character,index)=>{
		return {
			id: character.id,
			name: character.attributes.name,
			description: character.attributes.description,
			type: data[index].type,
			createdAt: character.attributes.createdAt,
			updatedAt: character.attributes.updatedAt,
			role: data[index].attributes.role,
			image: character.attributes.image ? character.attributes.image.original : false
		}
	});	

	return parsedCharacters;
}

export function getCharacters(type, nextPage = false){

	const url = nextPage || `${KITSU_API_URL}/${type}-characters?include=character&page[limit]=20&page[offset]=0`;

	const fetching = type === 'anime' ? GET_ANIME_CHARACTERS_FETCHING : GET_MANGA_CHARACTERS_FETCHING;
	const success = type === 'anime' ? GET_ANIME_CHARACTERS : GET_MANGA_CHARACTERS;
	const error = type === 'anime' ? GET_ANIME_CHARACTERS_ERROR : GET_MANGA_CHARACTERS_ERROR;

	return dispatch => {
		dispatch(parseToDispatch(fetching));
		axios.get(url).then(response => {
			const payload = { 
				characters:  parseCharacters(response.data), 
				nextPage: response.data.links.next || false,
				total: response.data.meta.count
			}
			dispatch(parseToDispatch(success, payload));
		}).catch(error => {
			dispatch(parseToDispatch(error,error.message));
		});	
	};
}

