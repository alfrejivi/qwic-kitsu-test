# QWIC - Kitsu Frontend Test

## Usage
Install dependencies
`yarn install`

Run the app in development mode
`yarn start`

Builds the app for production to the build folder
`yarn run build`

## Author
Freddy Jimenez: alfrejivi@gmail.com